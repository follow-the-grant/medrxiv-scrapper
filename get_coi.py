from lxml import html
import glob

def get_coi(article_file):
    article = open(article_file, 'r').read() 
    coi_start = 'Competing Interest Statement'
    coi_start_len = len(coi_start)
    h_article = html.fromstring(article)
    
    # Get CoI
    meta = h_article.find('.//meta[@name="DC.Description"]')
    if meta is not None:
        coi_idx = meta.attrib['content'].find(coi_start)
        coi_end_idx = meta.attrib['content'].find('###', coi_idx)
        coi_text = meta.attrib['content'][coi_idx + coi_start_len: coi_end_idx].replace('\n', '')
    else:
        coi_text = 'NONE'
    print ("{}\t{}".format(article_file, coi_text))

articles = glob.glob('article-*')
for a in articles:
    get_coi(a)

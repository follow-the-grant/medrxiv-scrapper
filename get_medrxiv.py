from lxml import html
import requests

total_requests = 16563
medrxiv_api = 'https://api.biorxiv.org/details/medrxiv/1000-01-01/2020-10-30/{}'
medrxiv_api[:-3]

for page in range(0, 16563, 100):
    f = open('medrxiv-{}'.format(page), 'wb')
    if page == 0:
        r = requests.get(medrxiv_api[:-3])
    else:
        r = requests.get(medrxiv_api.format(page))
    f.write(r.content)
    f.close()     


from lxml import html
import glob
import pickle

def get_article_metadata(article_file):
    afile = open(article_file, 'r').read()
    h = html.fromstring(afile)
    
    xpaths = {'title': './/meta[@name="citation_title"]',
               'abstract': './/meta[@name="citation_abstract"]',
               'date': './/meta[@name="citation_date"]', 
               'doi': './/meta[@name="citation_doi"]'
               }

    metadata = {}
    for name, xp in xpaths.items():
        tag = h.find(xp)
        if tag is not None:
            metadata[name] = h.find(xp).attrib['content']
        else:
            metadata[name] = ''

    return metadata

articles = glob.glob('article-*')

am = []
for a in articles:
    am += [get_article_metadata(a)]

pickle.dump(am, open('article_metadata.pickle', 'wb'))


import json
import glob

def get_articles(medrxiv_file):
    a = json.loads(open(medrxiv_file, 'r').read())
    for article in a['collection']:
        doi = article['doi']
        version = article['version']
        article_url = 'https://www.medrxiv.org/content/{}v{}'.format(doi, version)
        print(doi, article_url)

medrxiv_files = glob.glob('medrxiv-*')

for mf in medrxiv_files:
    get_articles(mf)

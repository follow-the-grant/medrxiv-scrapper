from lxml import html
import glob

def get_authors(article_file):
    article = open(article_file, 'r').read()
    h = html.fromstring(article)

    authors = [] 
    in_author = False 
    for e in h.findall('.//meta'): 
        if 'name' in e.attrib and e.attrib['name'] == 'citation_author': 
            if not in_author: 
                author = {e.attrib['name']: e.attrib['content']} 
                author['article'] = article_file
                in_author = True 
            else: 
                in_author = False 
                authors += [author] 
                 
        elif in_author: 
            author[e.attrib['name']] = e.attrib['content']

    return authors

articles = glob.glob('article-*')
all_a = []
all_a = [ ai for a in articles for ai in get_authors(a)]

import pickle

p = open('all_authors_pickle', 'wb')
pickle.dump(all_a, p)


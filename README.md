# Scrapper for medRxiv API

1. Run ```python get_medrxvi.py```. This is going to download all articles metadata using medrxiv API in batches of 100 articles.

2. Run ```python get_articles_url.py``` to get the list of url's from all the articles in the step 1.

3. Download the articles html's using wget or curl.

4. Extract the CoI (Conflict of Interest) from the downloaded articles running: ```python get_coi.py```
